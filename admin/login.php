<?php 
require_once('../settings.php');

?>
<!DOCTYPE html>
<html lang="tr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Api Panel</title>
    <link href="/admin/dist/normalize.css" rel="stylesheet" type="text/css">
    <link href="/admin/dist/login.css" rel="stylesheet" type="text/css">
</head>

<?php
if (@$_SESSION['auth'] == md5(AUTH_PASS)){
    header('Location: /admin');
    exit();
}

if ($_POST) {
    if (isset($_POST['email']) && isset($_POST['password'])) {
        $mail = $_POST['email'];
        $pass = $_POST['password'];
        if ($mail == AUTH_MAIL && $pass = AUTH_PASS){
            $_SESSION['auth'] = md5(AUTH_PASS);
            header('Location: /admin');
        } else {
            $errorMsg = 'Kullanıcı ya da parola hatalı!';
        }
    } else {
        $errorMsg = 'Kullanıcı ve parola yazılmalı!';
    }
}
?>

<body>
    <div class="login-root">
        <div class="box-root flex-flex flex-direction--column mainbox">

            <div class="box-root padding-top--24 flex-flex flex-direction--column">

                <div class="formbg-outer">
                    <form class="formbg" method="POST">
                        <div class="formbg-inner padding-horizontal--48">
                            <?php
                            if (isset($errorMsg)) {
                            ?>
                                <p class="err"><?php echo $errorMsg ?></p>
                            <?php
                            }
                            ?>
                            <span class="padding-bottom--15">Giriş yapın</span>
                            <form id="stripe-login">
                                <div class="field padding-bottom--24">
                                    <label for="email">E-Posta</label>
                                    <input type="email" name="email">
                                </div>
                                <div class="field padding-bottom--24">
                                    <div class="grid--50-50">
                                        <label for="password">Parola</label>
                                    </div>
                                    <input type="password" name="password">
                                </div>
                                <div class="field">
                                    <input type="submit" name="submit" value="Giriş Yap">
                                </div>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>