<?php
require_once('../settings.php');

authControl();

//$content = file_get_contents('../swagger.json');
$content = "{}";

?>
<!DOCTYPE HTML>
<html lang="tr">

<head>
  <meta charset="utf-8">

  <title>Api Ayarları</title>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
  <link href="/admin/dist/normalize.css" rel="stylesheet" type="text/css">
  <link href="/admin/dist/jsoneditor.css" rel="stylesheet" type="text/css">
  <link href="/admin/dist/custom.css" rel="stylesheet" type="text/css">
</head>

<body>
  <main>
    <div id="left">
      <div id="fileoperation">
        <button id="fileopen" class="btn btn-open">Dosya Aç</button>
        <button id="filesave" class="btn btn-save">Dosyayı Kaydet</button>
      </div>
      <div id="jsoneditor"></div>
    </div>

    <div id="right">
      <iframe id="preview" src="/api/cekpara" frameborder="0"></iframe>
    </div>
  </main>

  <div class="modal" id="modalopen">
    <div class="modal-out">
      <p class="modal-title">Dosya Aç</p>
      <span class="close"></span>
      <div class="modal-content">
        <?php
        $files = glob("../json/*.json");
        foreach ($files as $file) {
        ?>
          <p class="fileopenlink" href="<?php echo $file ?>"><?php echo basename($file, '.json') ?></p>
        <?php
        }
        ?>
      </div>
    </div>
  </div>

  <script src="/admin/dist/jquery.js"></script>
  <script src="/admin/dist/jsoneditor.js"></script>
  <script src="/admin/dist/custom.js"></script>
</body>

</html>