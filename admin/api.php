<?php
require_once('../settings.php');

authControl();

if (isset($_POST['function'])) {
    switch ($_POST['function']) {
        case 'fileopen':
            if (isset($_POST['filename'])) {
                $content = file_get_contents('../json/' . $_POST['filename'] . '.json');
                echo $content;
            } else {
                echo 'Dosya adı belirtilmeli!';
            }
            break;
        case 'filesave':
            if (isset($_POST['filename']) && isset($_POST['content'])) {
                try {
                    $file = fopen('../json/' . $_POST['filename'] . '.json', 'w') or die('Dosya oluşturulamadı');
                    $content = $_POST['content'];
                    fwrite($file, $content);
                    fclose($file);
                    echo 'ok';
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo 'Dosya adı ya da içerik belirtilmedi!';
            }
            break;
        default:
            echo 'Geçersiz fonksiyon!';
    }
} else {
    echo 'Fonksiyon adı belirtilmedi!';
}
