# Swagger Api İşlemleri

Swagger UI üzerine kurulmuş api test ortamı ve düzenleme seçenekleri sunar

## Installation

Ek paket kurulumu ya da veritabanı gerektirmez.


## Kullanımı

```bash
Çekpara api test arayüzü
http://swagger-api.test:81/api/cekpara

Yönetici giriş ekranı
http://swagger-api.test:81/admin/
```

## Yapılacaklar
- api/dosya şeklinde adres sonuna belirtme yapılmamış ana adrese girilmişse mevcut api listesi ile istenen api test arayüzüne ulaşma özelliği
- Yönetici tarafında yeni dosya açma
- Yönetici tarafında dosya silme
- Yönetici tarafında dosya kaydedilip önizleme yenilendiğinde sunucu seçimi ve authorize bilgisinin hatırlanması
- Düzenlenen dosyalar için yedekleme sistemi